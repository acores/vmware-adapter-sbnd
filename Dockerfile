FROM ubuntu:latest

RUN apt clean && apt update && apt -y install apt-transport-https wget tar npm

RUN wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" https://javadl.oracle.com/webapps/download/GetFile/1.8.0_271-b09/61ae65e088624f5aaa0b1d2d801acb16/linux-i586/jdk-8u271-linux-x64.tar.gz && \
        mkdir -p /opt/jvm && \
        cd /opt/jvm && \
        tar -xvzf /jdk-8u271-linux-x64.tar.gz && \
        echo -e '\
        PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/opt/jvm/jdk1.8.0_271/bin:/opt/jvm/jdk1.8.0_271/db/bin:/opt/jvm/jdk1.8.0_271/jre/bin"\n\
        J2SDKDIR="/opt/jvm/jdk1.8.0_271"\n\
        J2REDIR="/opt/jvm/jdk1.8.0_271/jre"\n\
        JAVA_HOME="/opt/jvm/jdk1.8.0_271"\n\
        DERBY_HOME="/opt/jvm/jdk1.8.0_271/db"\n\
        J2SDKDIR="/opt/jvm/jdk1.8.0_271"\n\
        J2REDIR="/opt/jvm/jdk1.8.0_271/jre*"\n\
        JAVA_HOME="/opt/jvm/jdk1.8.0_271"\n\
        DERBY_HOME="/opt/jvm/jdk1.8.0_271/db"\
        ' > /etc/environment && \
        update-alternatives --install "/usr/bin/java" "java" "/opt/jvm/jdk1.8.0_271/bin/java" 0 && \
        update-alternatives --install "/usr/bin/javac" "javac" "/opt/jvm/jdk1.8.0_271/bin/javac" 0 && \
        update-alternatives --set java /opt/jvm/jdk1.8.0_271/bin/java && \
        update-alternatives --set javac /opt/jvm/jdk1.8.0_271/bin/javac && \
        update-alternatives --list java && \
        update-alternatives --list javac && \
        java -version && \
        apt install maven -y
